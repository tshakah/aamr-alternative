if not data.raw.item["advanced-assembling-machine-returns"] then
  return
end

local aamr = data.raw["assembling-machine"]["advanced-assembling-machine-returns"]
local graphics_path = "__aamr-alternative__/graphics/"

-- icon
data.raw.item["advanced-assembling-machine-returns"].icon = graphics_path .. "icons/advanced-assembling-machine.png"
aamr.icon = graphics_path .. "icons/advanced-assembling-machine.png"

-- technology
data.raw.technology["automation-returns"].icon = graphics_path .. "technology/advanced-assembling-machine.png"

-- sound
aamr.working_sound.sound = {
  {
    filename = "__aamr-alternative__/sounds/advanced-assembling-machine.ogg",
    volume = 0.8
  },
}

if settings.startup["aamra-apply-vanilla-rebalance"] then
  -- make emissions roughly follow vanilla Factorio progression
  aamr.energy_source.emissions_per_minute = 0.5
  aamr.energy_usage = "3906kW"
end

-- remnant (only if the Krastorio one doesn't already exist)
if not data.raw.corpse["kr-medium-random-pipes-remnant"] then
  data:extend({
    {
      type = "corpse",
      name = "kr-medium-random-pipes-remnant",
      icon = graphics_path .. "remnant/remnants-icon.png",
      icon_size = 64,
      flags = { "placeable-neutral", "building-direction-8-way", "not-on-map" },
      selection_box = { { -3, -3 }, { 3, 3 } },
      tile_width = 9,
      tile_height = 9,
      selectable_in_game = false,
      subgroup = "remnants",
      order = "z[remnants]-a[generic]-b[medium]",
      time_before_removed = 60 * 60 * 20, -- 20 minutes
      final_render_layer = "remnants",
      remove_on_tile_placement = false,
      animation = make_rotated_animation_variations_from_sheet(1, {
        filename = graphics_path .. "remnant/medium-random-pipes-remnant.png",
        line_length = 1,
        width = 175,
        height = 175,
        frame_count = 1,
        direction_count = 1,
        hr_version = {
          filename = graphics_path .. "remnant/hr-medium-random-pipes-remnant.png",
          line_length = 1,
          width = 350,
          height = 350,
          frame_count = 1,
          direction_count = 1,
          scale = 0.5,
        },
      }),
    },
  })
end

aamr.corpse = "kr-medium-random-pipes-remnant"

-- graphics :)
aamr.animation = {
  layers = {
    {
      filename = graphics_path .. "entity/advanced-assembling-machine.png",
      priority = "high",
      width = 160,
      height = 160,
      frame_count = 1,
      repeat_count = 32,
      animation_speed = 0.25,
      shift = { 0, 0 },
      hr_version = {
        filename = graphics_path .. "entity/hr-advanced-assembling-machine.png",
        priority = "high",
        width = 320,
        height = 320,
        frame_count = 1,
        repeat_count = 32,
        animation_speed = 0.25,
        shift = { 0, 0 },
        scale = 0.5,
      },
    },
    {
      filename = graphics_path .. "entity/advanced-assembling-machine-w1.png",
      priority = "high",
      width = 64,
      height = 72,
      shift = { -1.02, 0.29 },
      frame_count = 32,
      line_length = 8,
      animation_speed = 0.1,
      hr_version = {
        filename = graphics_path .. "entity/hr-advanced-assembling-machine-w1.png",
        priority = "high",
        width = 128,
        height = 144,
        shift = { -1.02, 0.29 },
        frame_count = 32,
        line_length = 8,
        animation_speed = 0.1,
        scale = 0.5,
      },
    },
    {
      filename = graphics_path .. "entity/advanced-assembling-machine-steam.png",
      priority = "high",
      width = 40,
      height = 40,
      shift = { -1.2, -2.1 },
      frame_count = 32,
      line_length = 8,
      animation_speed = 1.5,
      hr_version = {
        filename = graphics_path .. "entity/hr-advanced-assembling-machine-steam.png",
        priority = "high",
        width = 80,
        height = 81,
        shift = { -1.2, -2.1 },
        frame_count = 32,
        line_length = 8,
        animation_speed = 1.5,
        scale = 0.5,
      },
    },
    {
      filename = graphics_path .. "entity/advanced-assembling-machine-sh.png",
      priority = "high",
      width = 173,
      height = 151,
      shift = { 0.32, 0.12 },
      frame_count = 1,
      repeat_count = 32,
      animation_speed = 0.1,
      draw_as_shadow = true,
      hr_version = {
        filename = graphics_path .. "entity/hr-advanced-assembling-machine-sh.png",
        priority = "high",
        width = 346,
        height = 302,
        shift = { 0.32, 0.12 },
        frame_count = 1,
        repeat_count = 32,
        animation_speed = 0.1,
        draw_as_shadow = true,
        scale = 0.5,
      },
    },
    {
      filename = graphics_path .. "entity/advanced-assembling-machine-w2.png",
      priority = "high",
      width = 19,
      height = 13,
      frame_count = 8,
      line_length = 4,
      repeat_count = 4,
      animation_speed = 0.1,
      shift = { 0.17, -1.445 },
      hr_version = {
        filename = graphics_path .. "entity/hr-advanced-assembling-machine-w2.png",
        priority = "high",
        width = 37,
        height = 25,
        frame_count = 8,
        line_length = 4,
        repeat_count = 4,
        animation_speed = 0.1,
        shift = { 0.17, -1.445 },
        scale = 0.5,
      },
    },
    {
      filename = graphics_path .. "entity/advanced-assembling-machine-w3.png",
      priority = "high",
      width = 12,
      height = 9,
      frame_count = 8,
      line_length = 4,
      repeat_count = 4,
      animation_speed = 0.1,
      shift = { 0.93, -2.05 },
      hr_version = {
        filename = graphics_path .. "entity/hr-advanced-assembling-machine-w3.png",
        priority = "high",
        width = 23,
        height = 15,
        frame_count = 8,
        line_length = 4,
        repeat_count = 4,
        animation_speed = 0.1,
        shift = { 0.93, -2.05 },
        scale = 0.5,
      },
    },
    {
      filename = graphics_path .. "entity/advanced-assembling-machine-w3.png",
      priority = "high",
      width = 12,
      height = 9,
      frame_count = 8,
      line_length = 4,
      repeat_count = 4,
      animation_speed = 0.1,
      shift = { 0.868, -0.082 },
      hr_version = {
        filename = graphics_path .. "entity/hr-advanced-assembling-machine-w3.png",
        priority = "high",
        width = 23,
        height = 15,
        frame_count = 8,
        line_length = 4,
        repeat_count = 4,
        animation_speed = 0.1,
        shift = { 0.868, -0.082 },
        scale = 0.5,
      },
    },
    {
      filename = graphics_path .. "entity/advanced-assembling-machine-w3.png",
      priority = "high",
      width = 12,
      height = 9,
      frame_count = 8,
      line_length = 4,
      repeat_count = 4,
      animation_speed = 0.1,
      shift = { 0.868, 0.552 },
      hr_version = {
        filename = graphics_path .. "entity/hr-advanced-assembling-machine-w3.png",
        priority = "high",
        width = 23,
        height = 15,
        frame_count = 8,
        line_length = 4,
        repeat_count = 4,
        animation_speed = 0.1,
        shift = { 0.868, 0.552 },
        scale = 0.5,
      },
    },
  },
}
